libgstsnesspc.so: src/gstsnesspc.c src/gstsnesspc.h
	$(CC) $(CFLAGS) -Wall -Wextra -pedantic -O0 -g $(shell pkg-config --cflags --libs gstreamer-audio-1.0) src/gstsnesspc.c -Wl,-soname,libgstsnesspc.so -shared -fPIC -o $@ -l snes_spc
clean:
	rm libgstsnesspc.so
