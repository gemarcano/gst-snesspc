// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020
#ifndef GSTSNESSPC_H_
#define GSTSNESSPC_H_

#include <gst/gst.h>

#include <snes_spc/spc.h>

#include <stdbool.h>

G_BEGIN_DECLS

#define GST_TYPE_SNESSPC (gst_snesspc_get_type())
G_DECLARE_FINAL_TYPE(GstSnesSpc, gst_snesspc, GST, SNESSPC, GstElement)

struct _GstSnesSpc
{
	GstElement element;
	GstAdapter *adapter;
	GstPad *srcpad;
	GstPad *sinkpad;

	SNES_SPC *emulator;
	SPC_Filter *filter;

	unsigned long long current_utime;

	bool infinite;
	int pitch_offset;
	bool real_time;
	double tempo;
	const char *path;
};

G_END_DECLS

#endif//GSTSNESSPC_H_
