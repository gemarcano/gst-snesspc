// SPDX-License-Identifier: LGPL-2.1-or-later OR GPL-2.0-or-later
// Copyright: Gabriel Marcano, 2020

#include <gst/gst.h>
#include <gst/audio/audio.h>
#include "gstsnesspc.h"
#include <math.h>

GST_DEBUG_CATEGORY_STATIC(gst_snesspc_debug);
#define GST_CAT_DEFAULT gst_snesspc_debug

enum
{
	PROP_INFINITE = 1,
	PROP_PITCH_OFFSET,
	PROP_TEMPO,
	PROP_REAL_TIME,
	PROP_PATH,
};

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE(
	"src",
	GST_PAD_SRC,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS(
		"audio/x-raw,"
		"format=(string)" GST_AUDIO_NE(S16) ","
		"layout=(string)interleaved,"
		"rate=(int)32000,"
		"channels=(int)2"
	)
);

// FIXME make this optional if no file property is given
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE(
	"sink",
	GST_PAD_SINK,
	GST_PAD_ALWAYS,
	GST_STATIC_CAPS("audio/x-spc")
);

#define gst_snesspc_parent_class parent_class
G_DEFINE_TYPE(GstSnesSpc, gst_snesspc, GST_TYPE_ELEMENT)

static void gst_snesspc_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec);
static void gst_snesspc_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec);

static gboolean gst_snesspc_sink_event(GstPad *pad, GstObject *parent, GstEvent *event);
static GstFlowReturn gst_snesspc_chain(GstPad *pad, GstObject *parent, GstBuffer *buf);
static gboolean gst_snesspc_query(GstPad *pad, GstObject *parent, GstQuery *query);
static GstStateChangeReturn gst_snesspc_change_state(GstElement *element, GstStateChange transition);
static void gst_snesspc_play(void *user_data);

static void gst_snesspc_finalize(GObject *gobject);

static void gst_snesspc_class_init(GstSnesSpcClass *klass)
{
	GObjectClass *gobject_class;
	GstElementClass *gstelement_class;

	gobject_class = (GObjectClass*)klass;
	gstelement_class = (GstElementClass*)klass;

	gobject_class->set_property = gst_snesspc_set_property;
	gobject_class->get_property = gst_snesspc_get_property;
	gobject_class->finalize = gst_snesspc_finalize;

	gstelement_class->change_state = GST_DEBUG_FUNCPTR(gst_snesspc_change_state);

	g_object_class_install_property(gobject_class, PROP_INFINITE, g_param_spec_boolean("infinite-playback", "inf", "Continue generating samples indefinitely", TRUE, G_PARAM_READWRITE));
	g_object_class_install_property(gobject_class, PROP_PITCH_OFFSET, g_param_spec_int("pitch-offset", "pitch", "Pitch offset to apply to the output, in semitones", -100, 100, 0, G_PARAM_READWRITE));
	g_object_class_install_property(gobject_class, PROP_TEMPO, g_param_spec_double("tempo", "tempo", "Ratio to multiply tempo by to change the tempo", 0.0, INFINITY, 1.0, G_PARAM_READWRITE));
	g_object_class_install_property(gobject_class, PROP_REAL_TIME, g_param_spec_boolean("real-time", "real-time", "Generate samples in real-time instead of running ahead as fast as possible", FALSE, G_PARAM_READWRITE));
	g_object_class_install_property(gobject_class, PROP_PATH, g_param_spec_string("path", "path", "Path to the .spc file to emulate", NULL, G_PARAM_READWRITE));

	gst_element_class_set_details_simple(gstelement_class, "SNES SPC700 source", "Decoder/Audio", "emulate the SNES SPC700 and generate audio samples", "Gabriel Marcano <gabemarcano@yahoo.com>");

	gst_element_class_add_pad_template(gstelement_class, gst_static_pad_template_get(&src_factory));
	gst_element_class_add_pad_template(gstelement_class, gst_static_pad_template_get(&sink_factory));
}

static void gst_snesspc_init(GstSnesSpc *elem)
{
	// Setup sinkpad
	elem->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
	gst_pad_use_fixed_caps(elem->sinkpad);

	gst_pad_set_event_function(elem->sinkpad, GST_DEBUG_FUNCPTR(gst_snesspc_sink_event));
	gst_pad_set_chain_function(elem->sinkpad, GST_DEBUG_FUNCPTR(gst_snesspc_chain));
	gst_element_add_pad(GST_ELEMENT(elem), elem->sinkpad);
	gst_pad_set_query_function(elem->sinkpad, GST_DEBUG_FUNCPTR(gst_snesspc_query));

	// Setup srcpad
	elem->srcpad = gst_pad_new_from_static_template(&src_factory, "src");
	gst_pad_use_fixed_caps(elem->srcpad);

	gst_element_add_pad(GST_ELEMENT(elem), elem->srcpad);
	gst_pad_set_query_function(elem->srcpad, GST_DEBUG_FUNCPTR(gst_snesspc_query));

	// Setup rest of element
	elem->adapter = gst_adapter_new();
	elem->emulator = spc_new();
	elem->filter = spc_filter_new();

	elem->current_utime = 0;

	elem->infinite = TRUE;
	elem->pitch_offset = 0;
	elem->tempo = 1.0;
	elem->real_time = FALSE;
	elem->path = NULL;
}

static void gst_snesspc_finalize(GObject *gobject)
{
	GstSnesSpc *elem = GST_SNESSPC(gobject);
	GST_DEBUG_OBJECT(elem, "In finalize");
	if (elem->path)
	{
		// Yes, this value is only assigned in set_property, and it is always
		// an allocated string. We store it as const char* since we don't want
		// to mess with whatever we received from the user.
		free((char*)elem->path);
		elem->path = NULL;
	}
	spc_filter_delete(elem->filter);
	spc_delete(elem->emulator);
	g_object_unref(elem->adapter);
}

static void gst_snesspc_set_property(GObject *object, guint prop_id, const GValue *value, GParamSpec *pspec)
{
	// FIXME should I be doing some locking, if these are to be accessed from a
	// task?
	GstSnesSpc *elem = GST_SNESSPC(object);
	switch (prop_id)
	{
	case PROP_INFINITE:
		elem->infinite = g_value_get_boolean(value);
	break;
	case PROP_PITCH_OFFSET:
		elem->pitch_offset = g_value_get_int(value);
	break;
	case PROP_TEMPO:
		elem->tempo = g_value_get_double(value);
	break;
	case PROP_REAL_TIME:
		elem->real_time = g_value_get_boolean(value);
	break;
	case PROP_PATH:
		elem->path = g_value_dup_string(value);
	break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static void gst_snesspc_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
{
	// FIXME should I be doing some locking, if these are to be accessed from a
	// task?
	GstSnesSpc *elem = GST_SNESSPC(object);
	switch (prop_id)
	{
	case PROP_INFINITE:
		g_value_set_boolean(value, elem->infinite);
	break;
	case PROP_PITCH_OFFSET:
		g_value_set_int(value, elem->pitch_offset);
	break;
	case PROP_TEMPO:
		g_value_set_double(value, elem->tempo);
	break;
	case PROP_REAL_TIME:
		g_value_set_boolean(value, elem->real_time);
	break;
	case PROP_PATH:
		g_value_set_string(value, elem->path);
	break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
	}
}

static gboolean gst_snesspc_sink_event(GstPad *pad, GstObject *parent, GstEvent *event)
{
	// FIXME need to implement locking, this can be called from a thread
	// separate from the task
	GstSnesSpc *elem = GST_SNESSPC(parent);
	gboolean ret = TRUE;

	GST_LOG_OBJECT(elem, "Received %s event: %" GST_PTR_FORMAT, GST_EVENT_TYPE_NAME(event), (void*)event);

	switch (GST_EVENT_TYPE(event))
	{
	case GST_EVENT_SEGMENT:
		// Don't forward any preceeding segment data from any previous
		// element-- we'll send our own when we're ready
		gst_event_unref(event);
	break;
	case GST_EVENT_EOS:
	// FIXME only do this if sink pad is active, in other words, if we
	// didn't receive a file property from the user
	{

		// Now we can extract data from the adapter
		if (gst_adapter_available(elem->adapter) < 0x10200)
		{
			GST_ERROR_OBJECT(elem, "Not enough data received for it to be an SPC file");
			gst_adapter_clear(elem->adapter);
			ret = gst_pad_event_default(elem->srcpad, parent, event);
			return ret;
		}

		GstBuffer *buffer = gst_adapter_take_buffer(elem->adapter, gst_adapter_available(elem->adapter));
		// Yay, we now have the SPC file

		GstMapInfo map;
		gst_buffer_map(buffer, &map, GST_MAP_READ);
		spc_err_t error = spc_load_spc(elem->emulator, map.data, map.size);
		GST_DEBUG_OBJECT(elem, "spc_load_spc result: %s", error);
		gst_buffer_unmap(buffer, &map);
		gst_buffer_unref(buffer);
		if (error)
		{
			GST_ERROR_OBJECT(elem, "SPC emulator failed to load SPC data: %s", error);
			ret = gst_pad_event_default(elem->srcpad, parent, event);
			return ret;
		}

		// Modify emulator based on parameters
		spc_set_pitch_offset(elem->emulator, elem->pitch_offset);
		spc_set_tempo(elem->emulator, elem->tempo * spc_tempo_unit);

		// We have successfully loaded the SPC file into the emulator at this
		// point. Prepare it for generating samples
		spc_clear_echo(elem->emulator);
		spc_filter_clear(elem->filter);

		// Tell downstream what our caps is
		GstCaps *caps = gst_pad_get_pad_template_caps(elem->srcpad);
		gst_pad_set_caps(elem->srcpad, caps);
		gst_pad_push_event(elem->srcpad, gst_event_new_caps(caps));
		gst_caps_unref(caps);

		// Emulator is ready to run, so start the task that will handle
		// generating samples
		GstSegment seg;
		gst_segment_init(&seg, GST_FORMAT_TIME);
		gst_pad_push_event(elem->srcpad, gst_event_new_segment(&seg));

		gst_pad_start_task(elem->srcpad, gst_snesspc_play, (void*)elem, NULL);
		gst_event_unref(event);
	}
	break;
	default:
		ret = gst_pad_event_default(pad, parent, event);
		break;
	}

	return ret;
}

static GstFlowReturn gst_snesspc_chain(GstPad *pad, GstObject *parent, GstBuffer *buffer)
{
	(void)pad;
	GstSnesSpc *elem = GST_SNESSPC(parent);
	GstAdapter *adapter = elem->adapter;
	gst_adapter_push(adapter, buffer);

	return GST_FLOW_OK;
}

static gboolean gst_snesspc_query(GstPad *pad, GstObject *parent, GstQuery *query)
{
	//GstSnesSpc *elem = GST_SNESSPC(parent);
	gboolean result;

	switch(GST_QUERY_TYPE(query))
	{
	case GST_QUERY_POSITION:
	case GST_QUERY_DURATION:
	break;
	case GST_QUERY_CAPS:
	{
		GstCaps *caps = gst_pad_get_pad_template_caps(pad);
		gst_query_set_caps_result(query, caps);
		gst_caps_unref(caps);
		result = TRUE;

	}
	break;
	default:
		result = gst_pad_query_default(pad, parent, query);
	}
	return result;
}

static GstStateChangeReturn gst_snesspc_change_state(GstElement *element, GstStateChange transition)
{
	GstStateChangeReturn result = GST_STATE_CHANGE_SUCCESS;
	GstSnesSpc *elem = GST_SNESSPC(element);

	switch(transition)
	{
	case GST_STATE_CHANGE_NULL_TO_READY:
	break;

	default:
	break;
	}

	result = GST_ELEMENT_CLASS(parent_class)->change_state(element, transition);
	if (result == GST_STATE_CHANGE_FAILURE)
		return result;

	switch (transition)
	{
	case GST_STATE_CHANGE_READY_TO_NULL:
		gst_pad_stop_task(elem->srcpad);
	break;
	default:
	break;
	}

	return result;
}

static void gst_snesspc_play(void *user_data)
{
	GstSnesSpc *elem = (GstSnesSpc*)user_data;
	GstPad *pad = elem->srcpad;
	GstFlowReturn flow_result;
	GstBuffer *out;
	const int NUM_SAMPLES = 1<<10;

	GstMapInfo map;

	out = gst_buffer_new_and_alloc(NUM_SAMPLES * 4);
	GST_BUFFER_TIMESTAMP(out) = elem->current_utime * 1000ULL;
	gst_buffer_map(out, &map, GST_MAP_WRITE);
	spc_err_t err = spc_play(elem->emulator, NUM_SAMPLES * 2, (short*)map.data);
	if (!err)
		spc_filter_run(elem->filter, (short*)map.data, NUM_SAMPLES * 2);

	gst_buffer_unmap(out, &map);
	if (err)
	{
		GST_ELEMENT_ERROR(elem, STREAM, DECODE, (NULL), ("Error generating samples: %s", err));
		gst_pad_pause_task(pad);
		gst_pad_push_event(pad, gst_event_new_eos());
		return;
	}

	flow_result = gst_pad_push(elem->srcpad, out);
	if (flow_result != GST_FLOW_OK)
	{
		GST_DEBUG_OBJECT(elem, "stopping pad task: %s", gst_flow_get_name(flow_result));
		gst_pad_pause_task(pad);
		if (flow_result == GST_FLOW_EOS)
		{
			gst_pad_push_event(pad, gst_event_new_eos());
		}
	}

	// FIXME some kind of termination condition?

	elem->current_utime += NUM_SAMPLES * 1000ULL / 32;

	return;
}

static gboolean plugin_init(GstPlugin *plugin)
{
	GST_DEBUG_CATEGORY_INIT(gst_snesspc_debug, "snesspc", 0, "SNES SPC700 source");
	return gst_element_register(plugin, "snesspc", GST_RANK_SECONDARY, GST_TYPE_SNESSPC);
}

#define PACKAGE "SNESSPC"

GST_PLUGIN_DEFINE(
	GST_VERSION_MAJOR,
	GST_VERSION_MINOR,
	snesspc,
	"SNES SPC700 plugin",
	plugin_init,
	"1",
	"LGPL",
	"SNES SPC plugin",
	"https://gitlab.com/gemarcano/gst-snesspc"
)
